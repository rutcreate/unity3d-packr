﻿using UnityEngine;
using System.Collections;

public class Collect : MonoBehaviour {

	public AudioClip good;
	public AudioClip bad;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.gameObject) {
			Score score = GameObject.Find("score").GetComponent<Score>();
			if (other.gameObject.name == "star(Clone)") {
				score.score += other.gameObject.GetComponent<Star>().score;
				AudioSource.PlayClipAtPoint(good, other.gameObject.transform.position);
			}
			else if (other.gameObject.name == "enemy(Clone)") {
				score.score += other.gameObject.GetComponent<Enemy>().score;
				AudioSource.PlayClipAtPoint(bad, other.gameObject.transform.position);
			}
		}
		
		Destroy(other.gameObject);
	}
}
