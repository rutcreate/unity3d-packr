﻿using UnityEngine;
using System.Collections;

public class Controller : MonoBehaviour {

	public float turnSpeed = 1.0f;
	public AudioClip audio;

	private Vector3 moveDirection;
	
	void Start () {
	
	}

	void Update () {
		Vector3 currentPosition = transform.position;
		if (Input.GetButton("Fire1")) {
			Vector3 moveToward = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			moveDirection = moveToward - currentPosition;
			moveDirection.z = 0;
			moveDirection.Normalize();
			
			float targetAngle = Mathf.Atan2(moveDirection.y, moveDirection.x) * Mathf.Rad2Deg;
			targetAngle += 90.0f;
			transform.rotation = 
				Quaternion.Slerp( transform.rotation, 
				                 Quaternion.Euler( 0, 0, targetAngle ), 
				                 turnSpeed * Time.deltaTime );
		}
	}

	void OnTriggerEnter2D(Collider2D other) {
		AudioSource.PlayClipAtPoint(audio, other.gameObject.transform.position);
		Destroy(other.gameObject);
	}
}
