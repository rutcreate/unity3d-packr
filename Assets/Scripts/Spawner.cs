﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {
	
	public GameObject target;
	public GameObject[] enemies;
	public GameObject[] stars;

	public float spawnEnemyDelay = 4.0f;
	public float spawnEnemyRate = 4.0f;
	public float spawnStarDelay = 2.0f;
	public float spawnStarRate = 3.0f;

	// Use this for initialization
	void Start () {
		InvokeRepeating("spawnEnemy", spawnEnemyDelay, spawnEnemyRate);
		InvokeRepeating("spawnStar", spawnStarDelay, spawnStarRate);
	}
	
	// Update is called once per frame
	void Update () {

	}

	void FixedUpdate() {
			
	}

	void spawnEnemy() {
		int enemyIndex = Random.Range(0, enemies.Length);
		GameObject enemy = (GameObject) Instantiate(enemies[enemyIndex], transform.position, transform.rotation);
		Enemy enemyScript = enemy.GetComponent<Enemy>();
		enemyScript.target = target;
	}

	void spawnStar() {
		int starIndex = Random.Range(0, enemies.Length);
		GameObject star = (GameObject) Instantiate(stars[starIndex], transform.position, transform.rotation);
		Star starScript = star.GetComponent<Star>();
		starScript.target = target;
	}

}
