﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

	public int score = -50;
	public float turnSpeed = 90.0f;
	public GameObject target;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		transform.Rotate(Vector3.back, turnSpeed * Time.deltaTime);
//		Vector3 endPoint = target.transform.position;
//		transform.position = Vector3.Lerp(startPoint, endPoint, 30.0f);
	}
}
