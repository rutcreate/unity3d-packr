﻿using UnityEngine;
using System.Collections;

public class Move : MonoBehaviour {

	public float moveSpeed = 10.0f;
	public float turnSpeed = 1.0f;
	public float force = 100.0f;

	private Vector3 moveDirection;
	private GameObject target;
	
	void Start () {
		target = GameObject.Find("eater1");

		if (target) {
			moveDirection = target.transform.position - transform.position;
			moveDirection.z = 0;
			moveDirection.Normalize();
			
			rigidbody2D.AddForce(moveDirection * force);
		}
	}
}
