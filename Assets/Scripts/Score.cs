﻿using UnityEngine;
using System.Collections;

public class Score : MonoBehaviour {

	public int score = 0;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		int newScore = score;
		if (score < 0) {
			newScore = 0;
			endGame();
		}

		guiText.text = "Score: " + newScore;
	}

	void endGame() {
		GameObject[] objects = GameObject.FindGameObjectsWithTag("Objects");
		foreach (GameObject obj in objects) {
			Destroy(obj);
		}

		GameObject[] spawners = GameObject.FindGameObjectsWithTag("Spawners");
		foreach (GameObject obj in spawners) {
			Destroy(obj);
		}

		GameObject.Find("endText").GetComponent<GUIText>().enabled = true;
		Destroy(GameObject.Find("eater1"));
	}
}
